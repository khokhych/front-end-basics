'use strict';

window.onload = function () {
  renderingСolumn('.left', '.right', 'static/data.json');
}


function parse(path) {

  let data = new XMLHttpRequest();

  data.open('GET', path, false);
  data.send();

  try {
    return JSON.parse(data.response);
  } catch (err) {
    console.log(err);
  }

}

function renderingСolumn(leftColumn, rightColumn, dataurl) {

  let arrowClassName = "after";

  let data = parse(dataurl);
  let left = document.querySelector(leftColumn);
  let right = document.querySelector(rightColumn);

  for (const key in data) {

    const element = data[key];

    let newElement = document.createElement("div");
    newElement.className = 'element';
    newElement.setAttribute('data-add', 'off');

    newElement.setAttribute('data-id', key);

    newElement.innerHTML =
      `<div class="element-cont">
      
          <h3 class="name">
            ${ element['name'] }
          </h3>

          <div class="author">
            ${ element['author'] }
          </div>

          <img class="img" 
            src=" ${ element['img'] } "
            alt=" ${ element['name'] } " 
          ></img>

          <div class="${arrowClassName}"></div>
      
        </div>`;

    if (localStorage.getItem('add')) {
      let arrStorage = localStorage.getItem('add').split(',');
      let indexId = arrStorage.indexOf(key);

      if (indexId != -1) {
        right.appendChild(newElement);
        continue;
      }

    }

    left.appendChild(newElement);

  }

  let arrows = document.querySelectorAll(`.${arrowClassName}`);

  function eventClick() {

    let parent = this.closest('.element');

    if (parent.getAttribute('data-add') === 'on') {

      left.appendChild(parent);
      parent.setAttribute('data-add', 'off');

      storage(parent.getAttribute('data-id'));
      return;
    }

    parent.setAttribute('data-add', 'on');
    right.appendChild(parent);

    storage(parent.getAttribute('data-id'));

  };

  for (let i = 0; i < arrows.length; i++) {

    const element = arrows[i];
    element.onclick = eventClick;

  }

  search();

}

function search() {

  let input = document.querySelector('input');

  input.oninput = inputText;

  var timer;

  function inputText() {
    clearTimeout(timer);
    let self = this;
    timer = setTimeout(function () {

      let elements = document.querySelectorAll('.author');

      for (let i = 0; i < elements.length; i++) {
        elements[i].closest('.element').hidden = false;
        if (-1 !== elements[i].innerText.indexOf(self.value)) {
          elements[i].closest('.element').hidden = false;
          continue;
        }
        elements[i].closest('.element').hidden = true;

      }
    }, 1000);
  }
}

function storage(id) {

  if (!localStorage.getItem('add')) {
    localStorage.setItem('add', id);
    return;
  }

  let arrStorage = localStorage.getItem('add').split(',');
  let indexId = arrStorage.indexOf(id);

  if (indexId === -1) {
    arrStorage.push(id);
    localStorage.setItem('add', arrStorage.join(','));
    return;
  }

  arrStorage.splice(indexId, 1);
  localStorage.setItem('add', arrStorage.join(','));

}